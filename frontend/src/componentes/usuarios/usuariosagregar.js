/*
import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import '../text_white.css';

function UsuariosAgregar()
{
    const[nombre, setNombre] = useState('')
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function usuariosInsertar()
    {

        const usuarioinsertar = {
            id: uniquid(),
            nombre: nombre,
            email: email,
            password: password,
            activo: activo
        }

        console.log(usuarioinsertar)

        axios.post(`/api/usuarios/agregar`,usuarioinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/tablausuarios')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function usuariosRegresar()
    {
        //window.location.href="/";
        navigate('/tablausuarios')
    }

    return(
        <div className="container mt-5">
            <h4>Nueva Categoría Producto</h4>
            <div className="row">
            <div class="col-sm-12 table-responsive">
            <table className="table table-inverse">
                <div className="col-md-12">
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>           
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email</label>
                        <input type="text" className="form-control" id="email" value={email} onChange={(e) => {setEmail(e.target.value)}}></input>
                    </div>  
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="text" className="form-control" id="password" value={password} onChange={(e) => {setPassword(e.target.value)}}></input>
                    </div>           
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Estado</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={usuariosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={usuariosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
                </table>
            </div>
            </div>
        </div>
    )

}

export default UsuariosAgregar;
*/