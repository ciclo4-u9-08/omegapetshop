import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function DetallePedidosEditar()
{
    const parametros = useParams()
    const[id_pedido, setIdPedido] = useState('')
    const[id_producto, setIdProducto] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const[valor, setValor] = useState('') 
    // const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/detallepedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/detallepedidos/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/detallepedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataDetallePedidos = res.data[0]
        setIdPedido(dataDetallePedidos.id_pedido)
        setIdProducto(dataDetallePedidos.id_producto)
        setCantidad(dataDetallePedidos.cantidad)
        setValor(dataDetallePedidos.valor)
        // setActivo(dataDetallePedidos.activo)
        })
    }, [])

    function detallepedidosActualizar()
    {
        const detallepedidoactualizar = {
            id: parametros.id,
            id_pedido: id_pedido,
            id_producto: id_producto,
            cantidad: cantidad,  
            valor: valor,     
            // activo: activo
        }

        console.log(detallepedidoactualizar)
        axios.post(`/api/detallepedidos/editar/${parametros.id}`,detallepedidoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/detallepedidoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function detallepedidosRegresar()
    {
        //window.location.href="/";
        navigate('/detallepedidoslistar')
    }


    return(
    
        <div className="container mt-5">
        <h4>Editar Detalle Pedidos</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">
                        <form>
                            {/* <div className="form-group">
                                <label htmlFor="id_pedido">Id Pedido</label>
                                <input type="text" className="form-control"id="id_pedido" value={id_pedido} onChange={(e)=>{setIdPedido(e.target.value)}}></input>
                            </div> */}
                            <div className="form-group">
                                <label htmlFor="id_producto">Id Producto</label>
                                <input type="text" className="form-control" id="id_producto" value={id_producto} onChange={(e)=>{setIdProducto(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="cantidad">Cantidad</label>
                                <input type="text" className="form-control"id="cantidad" value={cantidad} onChange={(e)=>{setCantidad(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="valor">Valor Total</label>
                                <input type="text" className="form-control"id="valor" value={valor} onChange={(e)=>{setValor(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={detallepedidosRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={detallepedidosActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>  
   
    )

}

export default DetallePedidosEditar;