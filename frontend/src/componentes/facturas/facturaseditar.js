import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function FacturasEditar()
{
    const parametros = useParams()
    const[id_pedido, setIdPedido] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/facturas/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/facturas/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/facturas/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataFacturas = res.data[0]
        setIdPedido(dataFacturas.id_pedido)
        setActivo(dataFacturas.activo)
        })
    }, [])

    function facturasActualizar()
    {
        const facturaactualizar = {
            id: parametros.id,
            id_pedido: id_pedido,   
            activo: activo
        }

        console.log(facturaactualizar)
        axios.post(`/api/facturas/editar/${parametros.id}`,facturaactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/facturaslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function facturasRegresar()
    {
        //window.location.href="/";
        navigate('/facturaslistar')
    }


    return(
    
        <div className="container mt-5">
        <h4>Editar Facturas</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="id_pedido">Id Pedido</label>
                                <input type="text" className="form-control" id="id_pedido" value={id_pedido} onChange={(e)=>{setIdPedido(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="activo">Estado</label>
                                <input type="text" className="form-control"id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={facturasRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={facturasActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>  
   
    )

}

export default FacturasEditar;