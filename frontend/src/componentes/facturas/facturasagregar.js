import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import '../text_white.css';

function FacturasAgregar()
{
    const[id_pedido, setIdPedido] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function facturasInsertar()
    {

        const facturainsertar = {
            id: uniquid(),
            id_pedido: id_pedido, 
            activo: activo
        }

        console.log(facturainsertar)

        axios.post(`/api/facturas/agregar`,facturainsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/facturaslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function facturasRegresar()
    {
        //window.location.href="/";
        navigate('/facturaslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nueva Factura</h4>
            <div className="row">
            <div class="col-sm-12 table-responsive">
            <table className="table table-inverse">
                <div className="col-md-12">
                    <div className="mb-3">
                        <label htmlFor="id_pedido" className="form-label">Id Pedido</label>
                        <input type="text" className="form-control" id="id_pedido" value={id_pedido} onChange={(e) => {setIdPedido(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Estado</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={facturasRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={facturasInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
                </table>
            </div>
            </div>
        </div>
    )

}

export default FacturasAgregar;