import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function ClientesEditar()
{
    const parametros = useParams()
    const[id, setId] = useState('')
    const[id_tipodocumento, setIdTipoDocumento] = useState('')
    const[nombre, setNombre] = useState('')
    const[telefono, setTelefono] = useState('')
    const[direccion, setDireccion] = useState('')
    const[email, setEmail] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/clientes/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/clientes/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/clientes/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataClientes = res.data[0]
        setId(dataClientes.id)
        setIdTipoDocumento(dataClientes.id_tipodocumento)
        setNombre(dataClientes.nombre)
        setTelefono(dataClientes.telefono)
        setDireccion(dataClientes.direccion)
        setEmail(dataClientes.email)
        setActivo(dataClientes.activo)
        })
    }, [])

    function clientesActualizar()
    {
        const clienteactualizar = {
            // id: parametros.id,
            id: id,
            id_tipodocumento: id_tipodocumento,
            nombre: nombre,
            telefono: telefono,
            direccion: direccion,
            email: email,
            activo: activo
        }

        console.log(clienteactualizar)
        axios.post(`/api/clientes/editar/${parametros.id}`,clienteactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/clienteslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function clientesRegresar()
    {
        //window.location.href="/";
        navigate('/clienteslistar')
    }


    return(
    
        <div className="container mt-5">
        <h4>Editar Clientes</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="id_tipodocumento">Num Documento</label>
                                <input type="text" className="form-control" id="id" value={id} onChange={(e)=>{setId(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="id_tipodocumento">Id Tipo Documento</label>
                                <input type="text" className="form-control" id="id_tipodocumento" value={id_tipodocumento} onChange={(e)=>{setIdTipoDocumento(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="telefono">Telefono</label>
                                <input type="text" className="form-control" id="telefono" value={telefono} onChange={(e)=>{setTelefono(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="direccion">Direccion</label>
                                <input type="text" className="form-control" id="direccion" value={direccion} onChange={(e)=>{setDireccion(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="text" className="form-control" id="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="activo">Estado</label>
                                <input type="text" className="form-control"id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={clientesRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={clientesActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>  
   
    )

}

export default ClientesEditar;