import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ClientesBorrar from './clientesborrar';
import '../text_white.css';

import Encabezado from '../encabezado';
import MenuLateral from '../menulateral';

//Metodo que contiene las tareas para listar clientes
function ClientesListar()
{

const[dataClientes, setdataClientes] = useState([])


   axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`;

   useEffect(()=>{
       axios.get('/api/clientes/listar').then(res => {
       console.log(res.data)
       setdataClientes(res.data)
       }).catch(err=>{console.log(err.stack)})
   },[])

    //Estructura para dibujar fondos de diversos colores en la tabla
    const tabla = document.getElementsByTagName("tr");
    //tabla[i].style.backgroundColor = "#888888";     //table- colores claros y bg - colores de contraste
    let i, j=0, fila=[], color=[], inicio=0;
    color[0]="bg-info";color[1]="";color[2]="bg-success";color[3]="";color[4]="bg-danger";color[5]="";color[6]="bg-warning";color[7]="";color[8]="bg-active";color[9]="";
    for(i=0;i<tabla.length;i++)
    {
        fila[i]=color[j];
        j++;
        if(j==10){j=0;}
    }

    return (
        <div className="">
        <Encabezado/>
        <MenuLateral/>
        <div className="content-wrapper">
        <div className= 'gray1'>
        <div className="container-fluid">
            <div className="row">
            <div className="col-sm-12 p-0">
                <div className="main-header">
                    <h4>Clientes</h4>
                    <ol className="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li className="breadcrumb-item">
                        <a href="index.html">
                            <i className="icofont icofont-home"></i>
                        </a>
                        </li>
                        <li className="breadcrumb-item"><a href="#">Clientes</a>
                        </li>
                        <li className="breadcrumb-item"><a href="basic-table.html">Lista de Clientes</a>
                        </li>
                    </ol>
                </div>
            </div>
            </div>
            </div>

<div className="row">
    <div className="col-sm-12">
        <div className="card">
        <div className= 'gray2'>
            <div className="card-header">
                <h5 className="card-header-text">LISTA DE CLIENTES</h5>
                                <Link to={`/clientesagregar`} className="btn btn-success btn-icon waves-effect waves-light">
                                    <i className="icofont icofont-ui-add"></i>
                                </Link>
            </div>
            <div className="card-block">
                <div className="row">
                <div className="col-sm-12 table-responsive">
                    <table className="table table-inverse">
                        <thead>
                        {/* <tr key={0}>
                                <td colSpan={6} align="right">
                                    <Link to={`/clientesagregar`} className="btn btn-success btn-icon waves-effect waves-light">
                                    <i className="icofont icofont-ui-add"></i>
                                    </Link>
                                </td>
                            </tr> */}
                            <tr key={0}>
                                <th align="center">Id Documento</th>
                                <th align="center">Id Tipo Documento</th>
                                <th align="center">Nombre</th> 
                                <th align="center">Telefono</th> 
                                <th align="center">Direccion</th> 
                                <th align="center">Email</th> 
                                <th align="center">Activo</th> 
                                <th align="center"></th>
                                <th align="center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            dataClientes.map((micliente, indice) => (
                            <tr className={`${fila[indice]}`} key={micliente.id}>
                                <td align="center">{micliente.id}</td> 
                                <td align="center">{micliente.id_tipodocumento}</td>  
                                <td align="center">{micliente.nombre}</td>
                                <td align="center">{micliente.telefono}</td>
                                <td align="center">{micliente.direccion}</td>
                                <td align="center">{micliente.email}</td>
                                <td align="center">{micliente.activo ? 'Activo' : 'Inactivo'}</td>
                                <td align="center"><Link to={`/clienteseditar/${micliente.id}`} className="btn btn-primary btn-icon waves-effect waves-light"><i className="icofont icofont-edit "></i></Link></td>
                                <td align="center">
                                <button type="button" onClick={()=>{ClientesBorrar(micliente.id)}} className="btn btn-danger btn-icon waves-effect waves-light" data-toggle="tooltip" data-placement="top" title=".icofont-code-alt">
                                <i className="icofont icofont-trash"></i>
                                </button>
                                </td>
                            </tr>
                        ))
                        }

                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>



        </div>
    </div>
    </div>
    )

}

export default ClientesListar;










