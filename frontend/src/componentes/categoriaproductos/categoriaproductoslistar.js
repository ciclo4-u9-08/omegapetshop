import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import CategoriaProductosBorrar from './categoriaproductosborrar';
import '../text_white.css';

import Encabezado from '../encabezado';
import MenuLateral from '../menulateral';

//Metodo que contiene las tareas para listar categoriaproductos
function CategoriaProductosListar()
{

const[dataCategoriaProductos, setdataCategoriaProductos] = useState([])


   axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`;

   useEffect(()=>{
       axios.get('/api/categoriaproductos/listar').then(res => {
       console.log(res.data)
       setdataCategoriaProductos(res.data)
       }).catch(err=>{console.log(err.stack)})
   },[])

    //Estructura para dibujar fondos de diversos colores en la tabla
    const tabla = document.getElementsByTagName("tr");
    //tabla[i].style.backgroundColor = "#888888";     //table- colores claros y bg - colores de contraste
    let i, j=0, fila=[], color=[], inicio=0;
    color[0]="bg-info";color[1]="";color[2]="bg-success";color[3]="";color[4]="bg-danger";color[5]="";color[6]="bg-warning";color[7]="";color[8]="bg-active";color[9]="";
    for(i=0;i<tabla.length;i++)
    {
        fila[i]=color[j];
        j++;
        if(j==10){j=0;}
    }

    return (
        <div className="">
        <Encabezado/>
        <MenuLateral/>
        <div className="content-wrapper">
        <div className= 'gray1'>
        <div className="container-fluid">
            <div className="row">
            <div className="col-sm-12 p-0">
                <div className="main-header">
                    <h4>Categoría Productos</h4>
                    <ol className="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li className="breadcrumb-item">
                        <a href="index.html">
                            <i className="icofont icofont-home"></i>
                        </a>
                        </li>
                        <li className="breadcrumb-item"><a href="#">Categoría Productos</a>
                        </li>
                        <li className="breadcrumb-item"><a href="basic-table.html">Lista de Categoría Productos</a>
                        </li>
                    </ol>
                </div>
            </div>
            </div>
            </div>

<div className="row">
    <div className="col-sm-12">
        <div className="card">
        <div className= 'gray2'>
            <div className="card-header">
                <h5 className="card-header-text">LISTA DE CATEGORÍA PRODUCTOS</h5>
                                <Link to={`/categoriaproductosagregar`} className="btn btn-success btn-icon waves-effect waves-light">
                                    <i className="icofont icofont-ui-add"></i>
                                </Link>
            </div>
            <div className="card-block">
                <div className="row">
                <div className="col-sm-12 table-responsive">
                    <table className="table table-inverse">
                        <thead>
                        {/* <tr key={0}>
                                <td colSpan={6} align="right">
                                    <Link to={`/categoriaproductosagregar`} className="btn btn-success btn-icon waves-effect waves-light">
                                    <i className="icofont icofont-ui-add"></i>
                                    </Link>
                                </td>
                            </tr> */}
                            <tr key={0}>
                                <th align="center">Id</th>
                                <th align="center">Nombre</th>
                                <th align="center">Estado</th> 
                                <th align="center"></th>
                                <th align="center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            dataCategoriaProductos.map((micategoriaproducto, indice) => (
                            <tr className={`${fila[indice]}`} key={micategoriaproducto.id}>
                                <td align="center">{micategoriaproducto.id}</td>  
                                <td align="center">{micategoriaproducto.nombre}</td>
                                <td align="center">{micategoriaproducto.activo ? 'Activo' : 'Inactivo'}</td>
                                <td align="center"><Link to={`/categoriaproductoseditar/${micategoriaproducto.id}`} className="btn btn-primary btn-icon waves-effect waves-light"><i className="icofont icofont-edit "></i></Link></td>
                                <td align="center">
                                <button type="button" onClick={()=>{CategoriaProductosBorrar(micategoriaproducto.id)}} className="btn btn-danger btn-icon waves-effect waves-light" data-toggle="tooltip" data-placement="top" title=".icofont-code-alt">
                                <i className="icofont icofont-trash"></i>
                                </button>
                                </td>
                            </tr>
                        ))
                        }

                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>



        </div>
    </div>
    </div>
    )

}

export default CategoriaProductosListar;










