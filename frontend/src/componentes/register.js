import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../utils/APIInvoke';
import swal from 'sweetalert';

const CrearCuenta = () => {

    const [usuario, setUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        confirmar: ''
    });

    const { nombre, email, password, confirmar } = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const crearCuenta = async () => {
        if (password !== confirmar) {
            const msg = "Las contraseñas son diferentes.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else if (password.length < 6) {
            const msg = "La contraseña deber ser al menos de 6 caracteres.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            const data = {
                nombre: usuario.nombre,
                email: usuario.email,
                password: usuario.password
            }
            const response = await APIInvoke.invokePOST(`/api/usuarios`, data);
            const mensaje = response.msg;

            if (mensaje === 'El usuario ya existe') {
                const msg = "El usuario ya existe.";
                swal({
                    title: 'Error',
                    text: msg,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        }
                    }
                });
            } else {
                const msg = "El usuario fue creado correctamente.";
                swal({
                    title: 'Información',
                    text: msg,
                    icon: 'success',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-primary',
                            closeModal: true
                        }
                    }
                });

                setUsuario({
                    nombre: '',
                    email: '',
                    password: '',
                    confirmar: ''
                })
            }
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearCuenta();
    }

    return (
        <div className="hold-transition login-page">
			<section className="login common-img-bg">   
			<div className="container-fluid">
			<div className="row">
				<div className="md-float-material">
					<div className="col-sm-12">
			<div className="login-card card-block bg-white">
			                    <div className="text-left">
									<img className="img-fluid" src="assets/images/logo-ops.png" alt="Theme-logo"></img>
								</div>
								<h3 className="text-center txt-primary">Crea Una Cuenta</h3>
                {/* <div className="login-logo">
                    <Link to={"#"}><b>Iniciar</b> Sesión</Link>
                </div> */}
                <div className="card">
                    <div className="card-body login-card-body">
					
                        {/* <p className="login-box-msg">Bienvenido, ingrese sus credenciales.</p> */}

											<form onSubmit={onSubmit}>
												<div className="md-input-wrapper">
													<input type="text"
														className="md-form-control" 
														// placeholder="Nombre"
														id="nombre"
														name="nombre"
														value={nombre}
														onChange={onChange}
														required
													/>
													<div className="md-input-wrapper">
														<div className="input-group-text">
															{/* <span className="fas fa-user" /> */}
														</div>
													</div>
													<label>Nombre</label>
												</div>

												<div className="md-input-wrapper">
													<input type="email"
														className="md-form-control"
														// placeholder="Email"
														id="email"
														name="email"
														value={email}
														onChange={onChange}
														required
													/>
													<div className="md-input-wrapper">
														<div className="input-group-text">
															{/* <span className="fas fa-envelope" /> */}
														</div>
													</div>
													<label>Email</label>
												</div>

												<div className="md-input-wrapper">
													<input type="password"
														className="md-form-control"
														// placeholder="Contraseña"
														id="password"
														name="password"
														value={password}
														onChange={onChange}
														required
													/>
													<div className="md-input-wrapper">
														<div className="input-group-text">
															{/* <span className="fas fa-lock" /> */}
														</div>
													</div>
													<label>Password</label>
												</div>

												<div className="md-input-wrapper">
													<input type="password"
														className="md-form-control"
														// placeholder="Confirmar Contraseña"
														id="confirmar"
														name="confirmar"
														value={confirmar}
														onChange={onChange}
														required
													/>
													<div className="md-input-wrapper">
														<div className="input-group-text">
															{/* <span className="fas fa-lock" /> */}
														</div>
													</div>
													<label>Confirmar Password</label>
												</div>
													<div className="rkmd-checkbox checkbox-rotate checkbox-ripple b-none m-b-20">
														<label className="input-checkbox checkbox-primary">
															<input type="checkbox" id="checkbox"></input>
															<span className="checkbox"></span>
														</label>
														<div className="captions">Recordarme</div>
													</div>

												<div className="col-xs-10 offset-xs-1">
												<button type="submit" className="btn btn-primary btn-md btn-block waves-effect waves-light m-b-20">
												Crear Cuenta
														</button>
													</div>
													<div className="row">
														<div className="col-xs-12 text-center">
															<span className="text-muted">Ya Tienes Una Cuenta?</span>
															<Link to={"/"} className="f-w-600 p-l-5">
																Iniciar Sesión
															</Link>

														</div>
													</div>
											</form>
										</div>
										
									</div>
									
								</div>
							
							{/* </form> */}
							
							</div>
							</div>
							{/* </form> */}
							</div>
							</div>
							</section>
							</div>
							
							
							
							// </div>
							// </div>
    );
}

export default CrearCuenta;