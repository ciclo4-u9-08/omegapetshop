// import logo from './logo.svg';
// import './App.css';
import React, {Fragment} from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

// import Menu from './componentes/menu';
import PaginaPrincipal from './componentes/paginaprincipal';

import ProductosListar from './componentes/productos/productoslistar';
import ProductosEditar from './componentes/productos/productoseditar';
import ProductosBorrar from './componentes/productos/productosborrar';
import ProductosAgregar from './componentes/productos/productosagregar';

import CategoriaProductosListar from './componentes/categoriaproductos/categoriaproductoslistar';
import CategoriaProductosEditar from './componentes/categoriaproductos/categoriaproductoseditar';
import CategoriaProductosBorrar from './componentes/categoriaproductos/categoriaproductosborrar';
import CategoriaProductosAgregar from './componentes/categoriaproductos/categoriaproductosagregar';

import PedidosListar from './componentes/pedidos/pedidoslistar';
import PedidosEditar from './componentes/pedidos/pedidoseditar';
import PedidosBorrar from './componentes/pedidos/pedidosborrar';
import PedidosAgregar from './componentes/pedidos/pedidosagregar';

import DetallePedidosListar from './componentes/detallepedidos/detallepedidoslistar';
import DetallePedidosEditar from './componentes/detallepedidos/detallepedidoseditar';
import DetallePedidosBorrar from './componentes/detallepedidos/detallepedidosborrar';
import DetallePedidosAgregar from './componentes/detallepedidos/detallepedidosagregar';

import FacturasListar from './componentes/facturas/facturaslistar';
import FacturasEditar from './componentes/facturas/facturaseditar';
import FacturasBorrar from './componentes/facturas/facturasborrar';
import FacturasAgregar from './componentes/facturas/facturasagregar';

import ClientesListar from './componentes/clientes/clienteslistar';
import ClientesEditar from './componentes/clientes/clienteseditar';
import ClientesBorrar from './componentes/clientes/clientesborrar';
import ClientesAgregar from './componentes/clientes/clientesagregar';

import UsuariosListar from './componentes/usuarios/usuarioslistar';
import UsuariosEditar from './componentes/usuarios/usuarioseditar';
import UsuariosBorrar from './componentes/usuarios/usuariosborrar';
import UsuariosAgregar from './componentes/usuarios/usuariosagregar';

import Login from './componentes/login';
import Register from './componentes/register';

function App() {
  return (
    <main className="App"> 
      <Fragment>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login/>} exact/>
            <Route path="/register" element={<Register/>} exact/>
            <Route path="/paginaprincipal" element={<PaginaPrincipal/>} exact/>

            <Route path="/productoslistar" element={<ProductosListar/>} exact/>
            <Route path="/productoseditar/:id" element={<ProductosEditar/>} exact/>
            <Route path="/productosborrar" element={<ProductosBorrar/>} exact/>
            <Route path="/productosagregar" element={<ProductosAgregar/>} exact/>

            <Route path="/categoriaproductoslistar" element={<CategoriaProductosListar/>} exact/>
            <Route path="/categoriaproductoseditar/:id" element={<CategoriaProductosEditar/>} exact/>
            <Route path="/categoriaproductosborrar" element={<CategoriaProductosBorrar/>} exact/>
            <Route path="/categoriaproductosagregar" element={<CategoriaProductosAgregar/>} exact/>

            <Route path="/pedidoslistar" element={<PedidosListar/>} exact/>
            <Route path="/pedidoseditar/:id" element={<PedidosEditar/>} exact/>
            <Route path="/pedidosborrar" element={<PedidosBorrar/>} exact/>
            <Route path="/pedidosagregar" element={<PedidosAgregar/>} exact/>

            <Route path="/detallepedidoslistar" element={<DetallePedidosListar/>} exact/>
            <Route path="/detallepedidoseditar/:id" element={<DetallePedidosEditar/>} exact/>
            <Route path="/detallepedidosborrar" element={<DetallePedidosBorrar/>} exact/>
            <Route path="/detallepedidosagregar" element={<DetallePedidosAgregar/>} exact/>

            <Route path="/facturaslistar" element={<FacturasListar/>} exact/>
            <Route path="/facturaseditar/:id" element={<FacturasEditar/>} exact/>
            <Route path="/facturasborrar" element={<FacturasBorrar/>} exact/>
            <Route path="/facturasagregar" element={<FacturasAgregar/>} exact/>

            <Route path="/clienteslistar" element={<ClientesListar/>} exact/>
            <Route path="/clienteseditar/:id" element={<ClientesEditar/>} exact/>
            <Route path="/clientesborrar" element={<ClientesBorrar/>} exact/>
            <Route path="/clientesagregar" element={<ClientesAgregar/>} exact/>

            <Route path="/usuarioslistar" element={<UsuariosListar/>} exact/>
            <Route path="/usuarioseditar/:id" element={<UsuariosEditar/>} exact/>
            <Route path="/usuariosborrar" element={<UsuariosBorrar/>} exact/>
            <Route path="/usuariosagregar" element={<UsuariosAgregar/>} exact/>

          </Routes>
        </BrowserRouter>
      </Fragment>
    </main>
  );
}

export default App;
