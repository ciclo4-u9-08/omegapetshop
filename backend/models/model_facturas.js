const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaFacturas = new miesquema({
    id: String,
    id_pedido: String,
    activo: Boolean    
})

const modeloFacturas = mongoose.model('facturas',esquemaFacturas);

module.exports= modeloFacturas; 