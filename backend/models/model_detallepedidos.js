const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaDetallePedidos = new miesquema({
    id: String,
    id_pedido: String,
    id_producto: String,
    cantidad: Number,
    valor: Number,
})

const modeloDetallePedidos = mongoose.model('detallepedidos',esquemaDetallePedidos);

module.exports= modeloDetallePedidos; 