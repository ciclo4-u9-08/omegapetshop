const mongoose = require("mongoose");

const usuariosSchema = mongoose.Schema({
  // id: String,
  nombre: { type: String, required: true, trim: true },
  email: { type: String, required: true, trim: true, unique: true },
  password: { type: String, required: true, trim: true },
  registro: { type: Date, default: Date.now() },
  activo: { type: Boolean, required: false, trim: true },
});

// module.exports = mongoose.model("usuarios", usuariosSchema);



const modeloUsuarios = mongoose.model("usuarios", usuariosSchema);
module.exports = modeloUsuarios;
